package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(CountRecursive(arr))
}

func CountRecursive(arr []int) int {
	if len(arr) == 0 {
		return 0
	} else if len(arr) == 1 {
		return 1
	} else {
		return 1 + CountRecursive(arr[1:])
	}
}
