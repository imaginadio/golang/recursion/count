package main

import (
	"testing"
)

var arr = []int{1, 2, 3, 4, 5}

func BenchmarkCountRecursive(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CountRecursive(arr)
	}
}
